package org.young.domain;

import java.util.Date;

public class MemberVO {

	private int no;
	private String userid;
	private String userpw;
	private String username;
	private String email;
	private String phone;
	private Date regdate;
	private Date updatedate;
	
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
	public String getUserpw() {
		return userpw;
	}
	public void setUserpw(String userpw) {
		this.userpw = userpw;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public Date getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}
	@Override
	public String toString() {
		return "MemberVO [no = " + no + ", userid=" + userid + ", userpw=" + userpw
		+ ", username=" + username + ", email=" + email + ", phone=" + phone
		+ ", regdate=" + regdate + ", updatedate=" + updatedate 
		+ ", getUserid()= " + getUserid() + ", getUserpw()= " + getUserpw()
		+ ", getUsername()=" + getUsername() + ", getEmail()= "	+ getEmail()
		+ ", getPhone()= "	+ getPhone() + ", getRegdate()=" + getRegdate()
		+ ", getUpdatedate()=" + getUpdatedate() + ", getClass()="
		+ getClass() + ", hashCode()=" + hashCode() + ", toString()="
		+ super.toString() + "]";
	}
}
